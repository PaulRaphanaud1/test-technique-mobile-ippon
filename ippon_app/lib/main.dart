import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:ippon_app/controller/graphql_controller.dart';
import 'package:ippon_app/utils/instance_binding.dart';
import 'package:ippon_app/views/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: graphqlClient,
      child: GetMaterialApp(
        initialBinding: InstanceBinding(),
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: "get_schwifty",
          primarySwatch: Colors.blue,
        ),
        home: const HomePage(title: 'Rick & Morty App'),
      ),
    );
  }
}
