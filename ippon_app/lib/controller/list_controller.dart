import 'package:get/get.dart';

class ListController extends GetxController {
  RxInt page = 1.obs;
  RxString filterGender = "".obs;
  RxString filterStatus = "".obs;
}
