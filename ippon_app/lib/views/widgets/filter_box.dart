import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ippon_app/controller/list_controller.dart';

class FilterBox extends StatefulWidget {
  const FilterBox({Key? key}) : super(key: key);

  @override
  _FilterBoxState createState() => _FilterBoxState();
}

class _FilterBoxState extends State<FilterBox> {
  String dropdownGender = Get.find<ListController>().filterGender.value;
  String dropdownStatus = Get.find<ListController>().filterStatus.value;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0))),
      insetPadding: const EdgeInsets.symmetric(horizontal: 24),
      title: const Text("Filter List"),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Gender"),
              const SizedBox(
                width: 20,
              ),
              DropdownButton<String>(
                value: dropdownGender,
                icon: const Icon(Icons.arrow_downward),
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                onChanged: (String? newValue) {
                  Get.find<ListController>().filterGender.value = newValue!;
                  Get.find<ListController>().page.value = 1;
                  setState(() {
                    dropdownGender = newValue;
                  });
                },
                items: <String>['Male', 'Female', '']
                    .map<DropdownMenuItem<String>>(
                  (String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  },
                ).toList(),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Status"),
              const SizedBox(
                width: 20,
              ),
              DropdownButton<String>(
                value: dropdownStatus,
                icon: const Icon(Icons.arrow_downward),
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                onChanged: (String? newValue) {
                  Get.find<ListController>().filterStatus.value = newValue!;
                  Get.find<ListController>().page.value = 1;
                  setState(() {
                    dropdownStatus = newValue;
                  });
                },
                items: <String>['Alive', 'Dead', 'unknown', '']
                    .map<DropdownMenuItem<String>>(
                  (String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  },
                ).toList(),
              ),
            ],
          ),
          ElevatedButton(
            onPressed: () {
              Get.find<ListController>().filterStatus.value = "";
              Get.find<ListController>().filterGender.value = "";
              Get.find<ListController>().page.value = 1;
              setState(() {
                dropdownStatus = "";
                dropdownGender = "";
              });
            },
            child: const Text('Reset filters'),
          ),
        ],
      ),
    );
  }
}
