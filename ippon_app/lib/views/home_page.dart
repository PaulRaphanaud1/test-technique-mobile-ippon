import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:ippon_app/controller/list_controller.dart';
import 'package:ippon_app/views/widgets/filter_box.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            onPressed: () => showDialog<String>(
              context: context,
              builder: (BuildContext context) {
                return SizedBox(
                    width: MediaQuery.of(context).size.width * 0.30,
                    child: const FilterBox());
              },
            ),
            icon: const Icon(Icons.filter_list),
          )
        ],
      ),
      body: Stack(
        children: [
          Obx(
            () => Query(
              options: QueryOptions(
                document: gql(
                  """
                  query {
                    characters(page: ${Get.find<ListController>().page}, filter: {status: "${Get.find<ListController>().filterStatus}", gender: "${Get.find<ListController>().filterGender}"}) {
                      info {
                        count
                        next
                        prev
                      }
                      results {
                        name
                        status
                        gender
                      }
                    }
                  }
                """,
                ),
              ),
              builder: (result, {FetchMore? fetchMore, refetch}) {
                // If stements here to check handle different states;
                if (result.isLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                int? nextPage = result.data?['characters']['info']['next'];
                int? prevPage = result.data?['characters']['info']['prev'];

                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text((prevPage != null) ? prevPage.toString() : ""),
                          IconButton(
                              onPressed: () {
                                if (prevPage != null) {
                                  Get.find<ListController>().page.value -= 1;
                                }
                              },
                              icon: const Icon(Icons.arrow_back)),
                          Text(
                            Get.find<ListController>().page.toString(),
                            style: const TextStyle(
                                fontSize: 18, fontFamily: 'get_schwifty'),
                          ),
                          IconButton(
                              onPressed: () {
                                if (nextPage != null) {
                                  Get.find<ListController>().page.value += 1;
                                }
                              },
                              icon: const Icon(Icons.arrow_forward)),
                          Text((nextPage != null) ? nextPage.toString() : ""),
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: result.data!['characters']['results'].length,
                        itemBuilder: (context, index) {
                          return ListTile(
                            title: Text(result.data!['characters']['results']
                                [index]['name']),
                            subtitle: Text(result.data!['characters']['results']
                                    [index]['status'] +
                                " " +
                                result.data!['characters']['results'][index]
                                    ['gender']),
                          );
                        },
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
          Positioned(
            bottom: 0,
            child: Image.asset(
              "assets/rickmorty.png",
              height: 200,
            ),
          ),
        ],
      ),
    );
  }
}
