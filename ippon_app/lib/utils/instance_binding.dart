import 'package:get/get.dart';
import 'package:ippon_app/controller/list_controller.dart';

class InstanceBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ListController>(() => ListController());
  }
}
