import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:ippon_app/main.dart';
import 'package:ippon_app/utils/instance_binding.dart';
import 'package:ippon_app/views/home_page.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'graphql_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.

const data = """
                  query {
                    characters(page: 0, filter: {status: "", gender: ""}) {
                      info {
                        count
                        next
                        prev
                      }
                      results {
                        name
                        status
                        gender
                      }
                    }
                  }
                """;

class MockGraphQLClient extends Mock implements GraphQLClient {}

@GenerateMocks([http.Client])
void main() {
  MockGraphQLClient mockClient;
  final HttpLink rickAndMortyHttpLink =
      HttpLink('https://rickandmortyapi.com/graphql');
  ValueNotifier<GraphQLClient> graphqlClient = ValueNotifier(
    GraphQLClient(
      link: rickAndMortyHttpLink,
      cache: GraphQLCache(
        store: InMemoryStore(),
      ),
    ),
  );
  setUp(() {
    mockClient = MockGraphQLClient();
  });

  group('fetch List', () {
    setUp(() {});
    // test('returns list of the GraphQL call ', () async {
    //   final client = MockClient();

    //   // Use Mockito to return a successful response when it calls the
    //   // provided http.Client.
    //   when(client.get(Uri.parse('https://rickandmortyapi.com/graphql')))
    //       .thenAnswer((_) async =>
    //           http.Response('{"userId": 1, "id": 2, "title": "mock"}', 200));

    //   expect("", "");
    // });

    Widget makeTestableWidget({Widget? child}) {
      return GraphQLProvider(
        client: graphqlClient,
        child: GetMaterialApp(
          initialBinding: InstanceBinding(),
          home: child,
        ),
      );
    }

    testWidgets('Find widget title', (WidgetTester tester) async {
      HomePage page = const HomePage(title: "testPage");
      await tester.pumpWidget(makeTestableWidget(child: page));
      // await tester.tap(find.byIcon(Icons.filter_list));
      final titleFinder = find.text('testPage');
      expect(titleFinder, findsOneWidget);
    });
  });
}
